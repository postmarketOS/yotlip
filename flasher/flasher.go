package flasher

import (
	"gitlab.com/postmarketOS/yotlip/pkgs/progress"
)

type Flasher interface {
	Flash(imagePath string, progress chan progress.Progress)
}

type flashProgress struct {
	total     uint64
	progress  chan progress.Progress
	totalSize uint64
}
