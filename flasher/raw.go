package flasher

type RawFlasher struct {
	Flasher
	// path to disk, e.g. /dev/sdc or \\.\Volume{b75e2c83-0000-0000-0000-602f00000000}
	// (TBD if that's actually useful on windows...)
	Disk string
}
