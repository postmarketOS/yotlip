package ui

import (
	"gioui.org/io/pointer"
	"gioui.org/layout"
	"gioui.org/op/clip"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"golang.org/x/image/draw"
	"image"
	"image/color"
	_ "image/png"
)

type ButtonTemplate interface {
	Layout(gtx C, th *material.Theme) D
	layoutButton(gtx C, th *material.Theme) D
}

type ImageButton struct {
	Button
	Img   image.Image
	Text  string
	imgOp paint.ImageOp
}

type TextButton struct {
	Button
	Text string
	Size unit.Value
}

type Button struct {
	ButtonTemplate
	X         int
	Y         int
	clicked   bool
	highlight bool
	button    widget.Clickable
}

func (b *ImageButton) layoutImage(gtx C) D {
	size := image.Point{X: b.Y, Y: b.Y}
	margins := layout.UniformInset(unit.Dp(10))

	return margins.Layout(gtx, func(C) D {
		if b.imgOp.Size().X != size.X {
			img := image.NewRGBA(image.Rectangle{Max: image.Point{X: b.X, Y: b.Y}})
			draw.ApproxBiLinear.Scale(img, img.Bounds(), b.Img, b.Img.Bounds(), draw.Src, nil)
			b.imgOp = paint.NewImageOp(img)
		}
		i := widget.Image{Src: b.imgOp}
		i.Scale = float32(size.X) / float32(gtx.Px(unit.Dp(float32(size.X))))
		return i.Layout(gtx)
	})
}

func (b *ImageButton) layoutButton(gtx C, th *material.Theme) D {
	return layout.Flex{
		Axis:      layout.Vertical,
		Alignment: layout.Middle,
		Spacing:   layout.SpaceEnd,
	}.Layout(gtx,
		// image
		layout.Rigid(b.layoutImage),
		// text label below
		layout.Rigid(material.Body1(th, b.Text).Layout),
	)
}

func (b *TextButton) layoutButton(gtx C, th *material.Theme) D {
	margins := layout.UniformInset(unit.Dp(10))
	if b.Size.V == 0 {
		// some default size if none specified
		b.Size = unit.Dp(24)
	}
	return margins.Layout(gtx, func(C) D {
		return layout.Flex{
			Axis:      layout.Vertical,
			Alignment: layout.Middle,
			Spacing:   layout.SpaceEnd,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				l := material.Label(th, b.Size, b.Text)
				// don't wrap text in the button
				l.MaxLines = 1
				return l.Layout(gtx)
			}),
		)
	})
}

func (b *Button) Clicked() bool {
	clicked := b.clicked
	b.clicked = false
	return clicked
}

// Layout an image button
func (b *Button) Layout(gtx C, th *material.Theme) D {
	for _, ev := range gtx.Events(b) {
		if x, ok := ev.(pointer.Event); ok {
			switch x.Type {
			case pointer.Press:
				b.clicked = true
				// unhighlight the button on click, since clicks might
				// change pages and we don't want the button to stay
				// highlighted if the user navigated back to the page
				// again
				b.highlight = false
			case pointer.Enter:
				b.highlight = true
			case pointer.Leave:
				b.highlight = false
			}
		}
	}

	var stack []layout.StackChild

	var dims layout.Dimensions
	stack = append(stack, layout.Stacked(func(gtx C) D {
		dims = layout.Flex{
			Axis:      layout.Vertical,
			Alignment: layout.Middle,
			Spacing:   layout.SpaceEnd,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				return b.layoutButton(gtx, th)
			}),
		)
		return dims
	}))

	if b.highlight {
		stack = append(stack, layout.Expanded(func(gtx C) D {
			rect := image.Rectangle{
				Max: dims.Size,
			}
			paint.FillShape(gtx.Ops, color.NRGBA{A: 50}, clip.Rect(rect).Op())
			return D{Size: dims.Size}
		}))
	}

	// reverse stack so highlight rect is below image/label
	var reversed []layout.StackChild
	for i := range stack {
		reversed = append(reversed, stack[len(stack)-i-1])
	}

	dims = layout.Stack{}.Layout(gtx, reversed...)

	// wire up pointer actions
	r := image.Rectangle{Max: image.Point{X: dims.Size.X, Y: dims.Size.Y}}
	area := pointer.Rect(r).Push(gtx.Ops)
	pointer.InputOp{
		Tag:   b,
		Types: pointer.Press | pointer.Enter | pointer.Leave,
	}.Add(gtx.Ops)
	area.Pop()

	return dims
}
