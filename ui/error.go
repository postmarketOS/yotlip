package ui

import (
	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget/material"
)

func ErrorLayout(gtx C, th *material.Theme, err error) D {
	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
		// https://todo.sr.ht/~eliasnaur/gio/285
		// Alignment: layout.Middle,
	}.Layout(gtx,
		layout.Rigid(func(gtx C) D {
			return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(C) D {
				return layout.Center.Layout(gtx, material.H3(th, ":(").Layout)
			})
		}),
		layout.Rigid(func(gtx C) D {
			return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(C) D {
				return layout.Center.Layout(gtx, material.Body1(th, err.Error()).Layout)
			})
		}),
		// TODO: include link to distro's help site
	)
}
