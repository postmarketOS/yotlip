package ui

import (
	"gioui.org/layout"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/pages/about"
	"golang.org/x/image/draw"
	"image"
)

type Header struct {
	app          *core.App
	th           *material.Theme
	distroLogo   image.Image
	distroLogoOp paint.ImageOp
	distroName   string
	aboutButton  TextButton
}

func NewHeader(app *core.App) *Header {
	ab := TextButton{
		Text: "about",
		Size: unit.Dp(14),
	}
	button := Button{
		X:              100,
		Y:              50,
		ButtonTemplate: &ab}
	ab.Button = button

	return &Header{
		app:         app,
		th:          app.Theme,
		distroLogo:  app.Distro.GetLogo(),
		distroName:  app.Distro.GetName(),
		aboutButton: ab,
	}
}

func (h *Header) Layout(gtx C, th *material.Theme) D {

	logoSize := image.Pt(75, 75)

	// used to constrain the text next to the logo
	var logoDims D

	// process any button clicks on page
	if h.aboutButton.Clicked() {
		h.app.Router.Pages["about"] = about.New(h.app)
		h.app.Router.Push("about")
		h.app.Window.Invalidate()
	}

	return layout.UniformInset(unit.Dp(6)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Horizontal,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			// distro logo
			layout.Rigid(func(gtx C) D {
				return layout.UniformInset(unit.Dp(10)).Layout(gtx, func(C) D {
					if h.distroLogoOp.Size().X != logoSize.X {
						img := image.NewRGBA(image.Rectangle{Max: logoSize})
						draw.ApproxBiLinear.Scale(img, img.Bounds(), h.distroLogo, h.distroLogo.Bounds(), draw.Src, nil)
						h.distroLogoOp = paint.NewImageOp(img)
					}
					i := widget.Image{Src: h.distroLogoOp}
					i.Scale = float32(logoSize.X) / float32(gtx.Px(unit.Dp(float32(logoSize.X))))
					logoDims = i.Layout(gtx)
					return logoDims
				})
			}),
			// app title
			layout.Rigid(func(gtx C) D {
				// Force the constraints to fit the button's height.
				gtx.Constraints.Min.Y = logoDims.Size.Y
				gtx.Constraints.Max.Y = logoDims.Size.Y
				// layout.Direction requires min constraints set along the direction axis
				return layout.S.Layout(gtx, material.H4(h.th, h.distroName+" Installer").Layout)
			}),
			// about button
			layout.Flexed(100, func(gtx C) D {
				gtx.Constraints.Min.Y = logoDims.Size.Y
				gtx.Constraints.Max.Y = logoDims.Size.Y
				return layout.S.Layout(gtx, func(gtx C) D {
					return layout.E.Layout(gtx, func(gtx C) D {
						return h.aboutButton.Layout(gtx, h.th)
					})
				})
			}),
		)
	})
}
