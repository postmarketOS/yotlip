package distro

import (
	"embed"
	"gitlab.com/postmarketOS/yotlip/config"
	"gitlab.com/postmarketOS/yotlip/pkgs/util"
	"image"
	"time"
)

type DistroTemplate interface {
	ReleasesPopulator

	SupportedDevices() []string
	DeviceSupported(device string) bool
	Image(ver string) // TODO: return what?
	ListUIs(r *Release) []*DistroUI
	ListReleases(device string) []*Release
	GetName() string
	GetLogo() image.Image
}

type Distro struct {
	DistroTemplate

	Config   config.Distro
	Releases map[string][]*Release
	Name     string
	Logo     image.Image
}

type ReleasesPopulator interface {
	PopulateReleases(cacheDir string) error
}

type Release struct {
	Name        string
	UIs         []*DistroUI
	DeviceKey   string
	Description string
}

type DistroUI struct {
	Name    string
	Photo   image.Image
	Image   *DistroImage
	Release *Release
}

type DistroImage struct {
	Name      string
	Url       string
	Sha512    string
	SizeBytes uint64
	Date      time.Time
	UI        *DistroUI
	Path      string
}

func (d *Distro) ListUIs(release *Release) []*DistroUI {
	for _, releases := range d.Releases {
		for _, r := range releases {
			if r.Name == release.Name {
				return r.UIs
			}
		}
	}

	return nil
}

func (d *Distro) ListReleases(device string) []*Release {
	return d.Releases[device]
}

func (d *Distro) DeviceSupported(device string) bool {
	_, ok := d.Releases[device]

	return ok
}

func (d *Distro) SupportedDevices() []string {
	devices := make([]string, len(d.Releases))

	i := 0
	for device := range d.Releases {
		devices[i] = device
		i++
	}

	return devices
}

func (d *Distro) GetName() string {
	return d.Name
}

func (d *Distro) GetLogo() image.Image {
	return d.Logo
}

func initDistro(n string, c *config.Config, res *embed.FS) (Distro, error) {
	d := Distro{
		Config: c.Distros[n],
		Name:   c.Distros[n].DisplayName,
	}
	logo, err := util.LoadImageFromRes("imgs/"+c.Distros[n].Logo, res)
	if err != nil {
		return d, err
	}
	d.Logo = logo
	return d, nil
}
