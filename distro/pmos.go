//go:build pmos

package distro

import (
	"embed"
	"encoding/json"
	"errors"
	"gitlab.com/postmarketOS/yotlip/config"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type PmOS struct {
	Distro
}

func New(c *config.Config, res *embed.FS) (*PmOS, error) {
	// TODO: don't take in embed.FS..

	d, err := initDistro("pmOS", c, res)
	if err != nil {
		return nil, err
	}
	return &PmOS{Distro: d}, nil
}

func (p *PmOS) PopulateReleases(cacheDir string) error {
	b, err := p.getReleasesIndex(cacheDir)
	if err != nil {
		return err
	}

	if err := p.unmarshalReleases(b); err != nil {
		return err
	}
	return nil
}

func (p *PmOS) Image(ver string) {

}

type pmosImage struct {
	Name      string  `json:"name"`
	Timestamp bpoTime `json:"timestamp"`
	Size      uint64  `json:"size"`
	URL       string  `json:"url"`
	Sha256    string  `json:"sha256"`
	Sha512    string  `json:"sha512"`
}

type pmosUI struct {
	Name   string      `json:"name"`
	Images []pmosImage `json:"images"`
}

type pmosDevice struct {
	Name       string   `json:"name"`
	Interfaces []pmosUI `json:"interfaces"`
}

type pmosRelease struct {
	Name    string       `json:"name"`
	Devices []pmosDevice `json:"devices"`
}

type pmosReleases struct {
	Releases []pmosRelease `json:"releases"`
}

type bpoTime struct {
	time.Time
}

// bpo's timestamp in the index.json is technically ISO 8601 compliant, since
// things should assume UTC if there's no tz data in the string, but the Go
// json parser doesn't think it is valid. This custom unmarshaler just assumes
// local time. This is technically wrong because bpo may not be located in UTC,
// but it's good enough for now.
func (t *bpoTime) UnmarshalJSON(b []byte) error {
	ret, err := time.Parse("2006-01-02T15:04:05", strings.ReplaceAll(string(b), "\"", ""))
	if err != nil {
		return err
	}
	t = &bpoTime{ret}
	return nil
}

func (p *PmOS) releaseDescription(releaseName string) (desc string) {
	for _, r := range p.Config.Releases {
		matched, _ := regexp.MatchString(strings.ReplaceAll(r.Regexp, "\\\\", "\\"), releaseName)
		if matched {
			return r.Description
		}
	}

	return desc
}

// Gets the latest stable release from a list of releases, assuming the format vX.Y (e.g. v21.06)
func (p *PmOS) latestStableRelease(releases []pmosRelease) (latest string) {
	var l float64

	for _, r := range releases {
		v, err := strconv.ParseFloat(strings.TrimPrefix(r.Name, "v"), 64)
		if err != nil {
			continue
		}
		if l == 0.0 || l < v {
			l = v
		}
	}

	latest = strconv.FormatFloat(l, 'f', 2, 64)
	return "v" + latest
}

func (p *PmOS) unmarshalReleases(data []byte) error {
	var releases pmosReleases
	if err := json.Unmarshal(data, &releases); err != nil {
		return err
	}

	p.Releases = make(map[string][]*Release)
	latestStable := p.latestStableRelease(releases.Releases)

	for _, rel := range releases.Releases {
		// skip any release that aren't edge or the latest stable
		if rel.Name != "edge" && rel.Name != latestStable {
			continue
		}
		for _, device := range rel.Devices {
			dName := device.Name
			release := Release{
				Name:        rel.Name,
				DeviceKey:   dName,
				Description: p.releaseDescription(rel.Name),
			}
			for _, ui := range device.Interfaces {
				u := DistroUI{
					Name:    ui.Name,
					Release: &release,
					// Photo: "imgs/" + ui.Name + ".png", // TODO: load image
				}
				var images []DistroImage
				for _, img := range ui.Images {
					i := DistroImage{
						Name:      img.Name,
						Url:       img.URL,
						Sha512:    img.Sha512,
						SizeBytes: img.Size,
						Date:      img.Timestamp.Time,
						UI:        &u,
					}
					images = append(images, i)
				}
				// prefer "installer" images if they exist
				images = *p.installerImages(&images)
				// get latest image for the release/device
				image, err := p.latestImage(&images)
				if err != nil {
					return err
				}
				u.Image = image
				release.UIs = append(release.UIs, &u)
			}
			p.Releases[dName] = append(p.Releases[dName], &release)
		}
	}

	return nil
}

func (p *PmOS) installerImages(images *[]DistroImage) *[]DistroImage {
	var installers []DistroImage

	for _, img := range *images {
		img := img
		if strings.Contains(img.Name, "-installer.") {
			installers = append(installers, img)
		}
	}
	if len(installers) > 0 {
		return &installers
	}
	return images
}

func (p *PmOS) latestImage(images *[]DistroImage) (*DistroImage, error) {

	var latest *DistroImage
	for _, i := range *images {
		i := i
		if latest == nil {
			latest = &i
		}
		if latest.Date.Before(i.Date) {
			latest = &i
		}
	}

	if latest == nil {
		return latest, errors.New("PmOS.latestImage: no images were given")
	}

	return latest, nil
}

func (p *PmOS) getReleasesIndex(dir string) (data []byte, err error) {
	indexFile := filepath.Join(dir, "index.json")

	stats, err := os.Stat(indexFile)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return data, err
	}

	// use index in cache if less than 6 hours old (arbitrarily picked time...)
	if errors.Is(err, os.ErrNotExist) || time.Now().Sub(stats.ModTime()).Hours() > 6 {
		url := p.Config.ImagesURL

		resp, err := http.Get(url)
		if err != nil {
			return data, err
		}
		defer resp.Body.Close()
		fd, err := os.Create(indexFile)
		if err != nil {
			return data, err
		}
		io.Copy(fd, resp.Body)
		fd.Close()
	}
	data, err = os.ReadFile(indexFile)
	if err != nil {
		return data, err
	}

	return
}
