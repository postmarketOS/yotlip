module gitlab.com/postmarketOS/yotlip

go 1.17

require (
	gioui.org v0.0.0-20211026101311-9cf7cc75f468
	gioui.org/x v0.0.0-20211027193954-ebdc19c7d07e
	git.sr.ht/~gioverse/skel v0.0.0-20211008142525-ecdaf33bb3a7
	github.com/godbus/dbus/v5 v5.0.5
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	gioui.org/cpu v0.0.0-20210817075930-8d6a761490d2 // indirect
	gioui.org/shader v1.0.6 // indirect
	github.com/gammazero/deque v0.1.0 // indirect
	github.com/gammazero/workerpool v1.1.2 // indirect
	golang.org/x/exp v0.0.0-20210722180016-6781d3edade3 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.6 // indirect
)
