package downloader

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"gitlab.com/postmarketOS/yotlip/pkgs/progress"
	"io"
	"log"
	"net/http"
	"os"
)

type Download struct {
	Url          string
	SaveFilePath string
	SizeBytes    uint64
	Sha512       string
}

// Downloads the file at Url to the SaveFilePath, and reports progress (as
// percent completed) over the given channel
func (d *Download) Start(prog chan progress.Progress) {
	defer close(prog)

	// use existing downloaded file if the checksum checks out
	c, _ := d.checksum()
	if c == d.Sha512 {
		log.Printf("downloader.Start: using existing downloaded file: %q", d.SaveFilePath)
		prog <- progress.Progress{Done: true}
		return
	}

	out, err := os.Create(d.SaveFilePath)
	if err != nil {
		err = fmt.Errorf("downloader.Start: unable to create save path: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}

	resp, err := http.Get(d.Url)
	if err != nil {
		out.Close()
		err = fmt.Errorf("downloader.Start: response failed: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	defer resp.Body.Close()

	counter := &progress.Counter{
		Progress: prog,
		Total:    d.SizeBytes,
	}
	if _, err = io.Copy(out, io.TeeReader(resp.Body, counter)); err != nil {
		out.Close()
		err = fmt.Errorf("downloader.Start: download failed: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	out.Sync()
	out.Close()

	checksum, err := d.checksum()
	if err != nil {
		err = fmt.Errorf("downloader.Start: unable to checksum file: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	if checksum != d.Sha512 {
		err = fmt.Errorf("downloaded file checksum does not match:\nexpected: %s\ngot: %s", d.Sha512, checksum)
		prog <- progress.Progress{Err: err}
		return
	}
}

func (d *Download) checksum() (string, error) {
	var sum string

	fd, err := os.Open(d.SaveFilePath)
	if err != nil {
		return sum, err
	}
	defer fd.Close()

	s := sha512.New()
	if _, err := io.Copy(s, fd); err != nil {
		return sum, err
	}

	sum = hex.EncodeToString(s.Sum(nil))
	return sum, nil
}
