package util

import (
	"embed"
	"image"
	"log"
)

func LoadImageFromRes(path string, res *embed.FS) (image.Image, error) {
	// TODO: take something more generic than embed.FS?
	var img image.Image

	data, err := res.Open(path)
	if err != nil {
		return img, err
	}
	defer data.Close()

	img, _, err = image.Decode(data)
	if err != nil {
		//TODO: don't Fatal out...
		log.Fatal(err)
	}

	return img, nil
}
