package fastcopy

import (
	"io"
)

// Copy routine that uses a large (1MB) buffer
func Copy(out io.Writer, in io.Reader) (bytesWritten int, err error) {

	buf := make([]byte, 1024000)
	var b int

	for {
		b, err = in.Read(buf)
		if err != nil {
			if err != io.EOF {
				break
			}
			// don't return EOF error
			err = nil
			break
		}
		bytesWritten += b
		out.Write(buf)
	}
	return
}
