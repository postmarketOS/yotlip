package progress

import (
	"time"
)

// Type sent over a channel to report progress and any errors
type Progress struct {
	Percent float32
	Err     error
	Done    bool

	// "spin" is true if the progress will wrap because the thing being
	// monitored cannot report actual progress
	Spin bool
}

// Progress is reported as a percentage of 1.0, based on the given total amount
// of bytes that are expected to be written, to the Progress channel. If Total
// is set to 0, progress still reflects bytes written but isn't a percentage of
// any total to be written since it's unknown, instead it wraps back to 0.0.
type Counter struct {
	totalWritten uint64
	Progress     chan<- Progress
	Total        uint64
}

// When used with io.TeeReader, will report current progress to the
// channel at Counter.Progress
func (c *Counter) Write(b []byte) (int, error) {
	size := len(b)
	c.totalWritten += uint64(size)

	c.Progress <- Progress{
		Percent: float32(float64(c.totalWritten) / float64(c.Total)),
		Done: c.totalWritten == c.Total,
	}

	return size, nil
}

type FakeCounter struct {
	Stop     <-chan bool
	Progress chan<- Progress
}

// A fake progress counter that just spins from 0 - 1.0 and back again until it
// receives 'true' over the FakeCounter.Stop channel
func (f *FakeCounter) Run() {
	i := 0

	for {
		if i >= 100 {
			i = 0
		}

		select {
		case <-f.Stop:
			f.Progress <- Progress{
				Percent: 1.0,
				Done:    true,
				Spin:    true,
			}
			return
		default:
			f.Progress <- Progress{
				Percent: float32(i) / 100.0,
				Spin:    true,
			}
		}
		time.Sleep(100 * time.Millisecond)
		i++
	}
}
