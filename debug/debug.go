package debug

import (
	"image/color"

	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget"
)

type (
	C = layout.Context
	D = layout.Dimensions
)

func Outline(gtx C, w func(gtx C) D) D {
	return widget.Border{
		Color: color.NRGBA{A: 255},
		Width: unit.Dp(1),
	}.Layout(gtx, w)
}
