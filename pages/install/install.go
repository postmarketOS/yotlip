package install

import (
	"context"
	"fmt"
	"log"
	"path/filepath"

	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget/material"
	"git.sr.ht/~gioverse/skel/scheduler"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/flasher"
	"gitlab.com/postmarketOS/yotlip/pkgs/downloader"
	"gitlab.com/postmarketOS/yotlip/pkgs/extractor"
	"gitlab.com/postmarketOS/yotlip/pkgs/progress"
	"gitlab.com/postmarketOS/yotlip/ui"
)

type Page struct {
	scheduler.Connection
	app        *core.App
	header     *ui.Header
	footer     *ui.Footer
	image      *distro.DistroImage
	flasher    flasher.Flasher
	extractor  extractor.Archive
	downloader downloader.Download

	flashing        bool
	flashingDone    bool
	extracting      bool
	extractingDone  bool
	downloading     bool
	downloadingDone bool

	progress      float32
	progressValid bool
	err           error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

func New(app *core.App, f flasher.Flasher, img *distro.DistroImage) *Page {
	p := &Page{
		app:        app,
		header:     ui.NewHeader(app),
		footer:     ui.NewFooter(app),
		Connection: app.Bus,
		flasher:    f,
		image:      img,
	}

	p.startDownloading()

	return p
}

type FlashProgress struct {
	progress.Progress
}

type ExtractProgress struct {
	progress.Progress
}

type DownloadImageProgress struct {
	downloader.Download
	progress.Progress
}

func (p *Page) Update(data interface{}) bool {
	switch data := data.(type) {
	case DownloadImageProgress:
		p.downloading = !data.Done
		p.downloadingDone = data.Done
		// update progress bar
		p.progress = data.Percent
		// don't consider progress to be valid if it's just spinning
		p.progressValid = !data.Spin
		if data.Err != nil {
			p.err = fmt.Errorf("error downloading image: %w", data.Err)
		} else if data.Done {
			log.Print("image downloaded: ", data.SaveFilePath)
			p.startExtracting()
		}
		return true
	case ExtractProgress:
		p.extracting = !data.Done
		p.extractingDone = data.Done
		// update progress bar
		p.progress = data.Percent
		// don't consider progress to be valid if it's just spinning
		p.progressValid = !data.Spin
		if data.Err != nil {
			p.err = fmt.Errorf("error extracting image: %w", data.Err)
		} else if data.Done {
			p.startFlashing()
		}
		return true
	case FlashProgress:
		p.flashing = !data.Done
		p.flashingDone = data.Done
		// update progress bar
		p.progress = data.Percent
		// don't consider progress to be valid if it's just spinning
		p.progressValid = !data.Spin
		if data.Err != nil {
			p.err = fmt.Errorf("error flashing image: %w", data.Err)
		} else if data.Done {
			// done flashing
		}
		return true
	}
	return false
}

func (p *Page) startDownloading() {
	var pCh = make(chan progress.Progress)
	p.image.Path = filepath.Join(p.app.CacheDir, p.image.Name)
	p.downloading = true
	p.ScheduleCtx(context.Background(), func(ctx context.Context) interface{} {
		log.Print("Downloading image: ", p.image.Url)
		d := downloader.Download{
			Url:          p.image.Url,
			SaveFilePath: p.image.Path,
			SizeBytes:    p.image.SizeBytes,
			Sha512:       p.image.Sha512,
		}
		go d.Start(pCh)
		for prog := range pCh {
			// forward download progress to message bus
			p.app.Bus.Message(DownloadImageProgress{
				Progress: prog,
				Download: d,
			})
		}
		// avoid double-posting last progress by returning nil
		return nil
	})
}

func (p *Page) startFlashing() {
	pCh := make(chan progress.Progress)
	p.ScheduleCtx(context.Background(), func(ctx context.Context) interface{} {
		go p.flasher.Flash(filepath.Join(filepath.Dir(p.image.Path), "uncompressed.img"), pCh)
		for prog := range pCh {
			// forward flashing progress to message bus
			p.app.Bus.Message(FlashProgress{
				Progress: prog,
			})
		}
		// avoid double-posting last progress by returning nil
		return nil
	})
}

func (p *Page) startExtracting() {
	pCh := make(chan progress.Progress)
	p.ScheduleCtx(context.Background(), func(ctx context.Context) interface{} {
		archive := extractor.Archive{
			InFile: p.image.Path,
			// the file name isn't important, any existing file is overwritten
			OutFile: filepath.Join(filepath.Dir(p.image.Path), "uncompressed.img"),
		}
		go archive.Extract(pCh)
		for prog := range pCh {
			p.app.Bus.Message(ExtractProgress{
				Progress: prog,
			})
		}
		// avoid double-posting last progress by returning nil
		return nil
	})
}

func (p *Page) layoutStep(gtx C, text string, running bool, done bool) D {
	sections := []layout.FlexChild{}

	if running || done {
		sections = append(sections, layout.Flexed(30, func(gtx C) D {
			return layout.W.Layout(gtx, func(gtx C) D {
				return layout.UniformInset(unit.Dp(10)).Layout(gtx, material.H6(p.app.Theme, text).Layout)
			})
		}))
	}

	if done {
		sections = append(sections, layout.Flexed(40, func(gtx C) D {
			return layout.W.Layout(gtx, func(gtx C) D {
				return layout.UniformInset(unit.Dp(10)).Layout(gtx, material.H6(p.app.Theme, "Done.").Layout)
			})
		}))
	} else if running {
		sections = append(sections, layout.Flexed(40, func(gtx C) D {
			return layout.W.Layout(gtx, func(gtx C) D {
				if p.progressValid {
					return layout.UniformInset(unit.Dp(10)).Layout(gtx, material.H6(p.app.Theme, fmt.Sprintf("%0.1f%%", p.progress*100.0)).Layout)
				}
				return layout.UniformInset(unit.Dp(10)).Layout(gtx, material.Loader(p.app.Theme).Layout)
			})
		}))
	} else {
		sections = append(sections, layout.Flexed(40, func(gtx C) D {
			return layout.W.Layout(gtx, func(gtx C) D {
				return layout.UniformInset(unit.Dp(10)).Layout(gtx, layout.Spacer{}.Layout)
			})
		}))
	}

	return layout.Flex{
		Axis:    layout.Horizontal,
		Spacing: layout.SpaceAround,
	}.Layout(gtx, sections...)
}

func (p *Page) Layout(gtx C) D {
	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return p.layoutStep(gtx, "Downloading image...", p.downloading, p.downloadingDone)
			}),
			layout.Rigid(func(gtx C) D {
				return p.layoutStep(gtx, "Extracting image...", p.extracting, p.extractingDone)
			}),
			layout.Rigid(func(gtx C) D {
				return p.layoutStep(gtx, "Flashing image...", p.flashing, p.flashingDone)
			}),
		}...)
		// progress bar
		if p.downloadingDone && p.extractingDone && p.flashingDone {
			// installation done
			sections = append(sections, layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Vertical,
					Spacing: layout.SpaceAround,
				}.Layout(gtx,
					layout.Rigid(func(gtx C) D {
						return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
							return layout.Center.Layout(gtx, material.H4(p.app.Theme, "Everything completed successfully \\o/").Layout)
						})
					}),
					layout.Rigid(func(gtx C) D {
						return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
							return layout.Center.Layout(gtx, material.H6(p.app.Theme, "Reboot your device to complete installation").Layout)
						})
					}),
				)
			}))
		}
		// footer
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return p.footer.Layout(gtx, p.app.Theme)
		}))
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
		// https://todo.sr.ht/~eliasnaur/gio/285
		// Alignment: layout.Middle,
	}.Layout(gtx, sections...)
}
