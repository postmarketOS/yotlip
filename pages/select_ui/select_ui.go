package select_ui

import (
	"fmt"
	"gioui.org/font/gofont"
	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/richtext"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/pages/confirm"
	"gitlab.com/postmarketOS/yotlip/ui"
	"image/color"
	"log"
)

type Page struct {
	app           *core.App
	uiButtons     []*uiButton
	header        *ui.Header
	footer        *ui.Footer
	scrollWrap    *ui.ScrollWrap
	backButton    widget.Clickable
	richTextState richtext.InteractiveText
	uis           []*distro.DistroUI
	err           error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

type uiButton struct {
	ui.ImageButton
	iface *distro.DistroUI
}

func New(app *core.App, rel *distro.Release) *Page {
	p := &Page{
		app:        app,
		header:     ui.NewHeader(app),
		footer:     ui.NewFooter(app),
		scrollWrap: &ui.ScrollWrap{Theme: app.Theme},
		uis:        rel.UIs,
	}

	return p
}

func (p *Page) Update(data interface{}) bool {
	// stub
	return false
}

// Layout the left-hand side (LHS) of the page body
func (p *Page) layoutLHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		// used to constrain the text next to the back button
		// var backButtonDims D
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
			// https://todo.sr.ht/~eliasnaur/gio/285
			// Alignment: layout.Middle,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(8)}.Layout(gtx,
					material.H5(p.app.Theme, "3. Select a User Interface:").Layout)
			}),
			// list of UIs
			layout.Flexed(100, func(gtx C) D {
				var children []func(gtx C) D
				for _, c := range p.uiButtons {
					c := c
					children = append(children, func(gtx C) D {
						return c.Layout(gtx, p.app.Theme)
					})
				}
				p.scrollWrap.Items = children
				return p.scrollWrap.Layout(gtx)
			}),
			// back button
			layout.Rigid(func(gtx C) D {
				gtx.Constraints.Min.X = 75
				gtx.Constraints.Max.X = 75
				return layout.Inset{Bottom: unit.Dp(10), Top: unit.Dp(20)}.Layout(gtx, material.Button(p.app.Theme, &p.backButton, "<- Back").Layout)
			}),
		)
	})
}

// Layout the right-hand side (RHS) of the page body
func (p *Page) layoutRHS(gtx C) D {

	font := gofont.Collection()[0].Font
	normalFont := font
	boldFont := font
	boldFont.Weight = text.Bold

	var spans []richtext.SpanStyle = []richtext.SpanStyle{
		{
			// TODO: improve this text, and put strings like this in some central location?
			Content: "These are the graphical user interfaces (UI) supported by " + p.app.Distro.GetName() + " ",
			Size:    unit.Dp(18),
			Font:    normalFont,
			Color:   color.NRGBA{A: 255},
		},
		{
			Content: p.uis[0].Release.Name,
			Size:    unit.Dp(18),
			Font:    boldFont,
			Color:   color.NRGBA{A: 255},
		},
		{
			Content: " for the:\n",
			Size:    unit.Dp(18),
			Font:    normalFont,
			Color:   color.NRGBA{A: 255},
		},
		{
			Content: p.app.Config.Devices[p.uis[0].Release.DeviceKey].DisplayName + ".",
			Size:    unit.Dp(18),
			Font:    boldFont,
			Color:   color.NRGBA{A: 255},
		},
	}

	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Flexed(100, richtext.Text(&p.richTextState, p.app.Theme.Shaper, spans...).Layout),
			// TODO: add support for distro release descriptions, and list them here:
		)
	})
}

func (p *Page) Layout(gtx C) D {
	if len(p.uis) == 0 {
		p.err = fmt.Errorf("select_ui: no user interfaces found for this device & release")
	}

	// process any button clicks on page
	if p.backButton.Clicked() {
		p.app.Router.Pop()
	}

	if len(p.uiButtons) == 0 {
		// if a valid UI was given on the cmdline, use it
		// this check is done here so that it happens only once instead of on
		// every frame redraw
		if *p.app.Options["ui"] != "" {
			var iface *distro.DistroUI
			for _, u := range p.uis {
				if u.Name == *p.app.Options["ui"] {
					iface = u
					break
				}
			}
			if iface != nil {
				log.Printf("pages/select_UI: auto-selecting UI from commandline: %q", *p.app.Options["ui"])
				p.app.Router.Pages["confirm"] = confirm.New(p.app, iface)
				p.app.Router.Push("confirm")
				p.app.Window.Invalidate()
			} else {
				log.Printf("pages/select_UI: ignoring unsupported/unknown UI set on commandline: %q", *p.app.Options["ui"])
			}
		}

		for _, iface := range p.uis {
			ub := uiButton{iface: iface}
			ub.Text = p.app.Config.UIs[iface.Name].DisplayName
			ub.Img = iface.Photo
			button := ui.Button{
				X:              125,
				Y:              240,
				ButtonTemplate: &ub,
			}
			ub.Button = button
			p.uiButtons = append(p.uiButtons, &ub)
		}
	}

	// process any button clicks on page
	for _, button := range p.uiButtons {
		if button.Clicked() {
			p.app.Router.Pages["confirm"] = confirm.New(p.app, button.iface)
			p.app.Router.Push("confirm")
			p.app.Window.Invalidate()
			break
		}
	}

	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Horizontal,
					Spacing: layout.SpaceBetween,
				}.Layout(gtx,
					// UI content left hand side
					layout.Flexed(60, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutLHS)
					}),
					// UI content right hand side
					layout.Flexed(40, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutRHS)
					}),
				)
			}),
			// footer
			layout.Flexed(100, func(gtx C) D {
				return p.footer.Layout(gtx, p.app.Theme)
			}),
		}...)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx, sections...)
}
