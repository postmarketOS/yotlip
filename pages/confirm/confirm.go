package confirm

import (
	"fmt"
	"gioui.org/font/gofont"
	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/richtext"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/pages/preflash/raw"
	"gitlab.com/postmarketOS/yotlip/ui"
	"image/color"
)

type Page struct {
	app           *core.App
	header        *ui.Header
	footer        *ui.Footer
	confirmButton widget.Clickable
	backButton    widget.Clickable
	richTextState richtext.InteractiveText
	uiface        *distro.DistroUI
	err           error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

func New(app *core.App, uiface *distro.DistroUI) *Page {
	p := &Page{
		app:    app,
		header: ui.NewHeader(app),
		footer: ui.NewFooter(app),
		uiface: uiface,
	}

	return p
}

func (p *Page) layoutConfirmListItem(gtx C, field string, value string) D {
	font := gofont.Collection()[0].Font
	boldFont := font
	boldFont.Weight = text.Bold

	return layout.Flex{
		Axis:    layout.Horizontal,
		Spacing: layout.SpaceAround,
	}.Layout(gtx,
		layout.Flexed(50, func(gtx C) D {
			return layout.E.Layout(gtx, func(gtx C) D {
				return layout.UniformInset(unit.Dp(10)).Layout(gtx,
					material.Label(p.app.Theme, unit.Dp(18), field).Layout)
			})
		}),
		layout.Flexed(50, func(gtx C) D {
			return layout.W.Layout(gtx, func(gtx C) D {
				l := material.Label(p.app.Theme, unit.Dp(18), value)
				l.Font.Weight = text.Bold
				return layout.UniformInset(unit.Dp(10)).Layout(gtx,
					l.Layout)
			})
		}),
	)
}

func (p *Page) Update(data interface{}) bool {
	// stub
	return false
}

func (p *Page) layoutConfirmList(gtx C) D {
	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx,
		layout.Rigid(func(gtx C) D {
			return p.layoutConfirmListItem(gtx, "Device",
				p.app.Config.Devices[p.uiface.Release.DeviceKey].DisplayName)
		}),
		layout.Rigid(func(gtx C) D {
			return p.layoutConfirmListItem(gtx, p.app.Distro.GetName()+" Release", p.uiface.Release.Name)
		}),
		layout.Rigid(func(gtx C) D {
			return p.layoutConfirmListItem(gtx, "User Interface", p.app.Config.UIs[p.uiface.Name].DisplayName)
		}),
		layout.Rigid(func(gtx C) D {
			return layout.UniformInset(unit.Dp(40)).Layout(gtx, material.Button(p.app.Theme, &p.confirmButton, "Confirm").Layout)
		}),
	)
}

// Layout the left-hand side (LHS) of the page body
func (p *Page) layoutLHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(8)}.Layout(gtx,
					material.H5(p.app.Theme, "Confirm selections:").Layout)
			}),
			// release buttons
			layout.Flexed(100, p.layoutConfirmList),
			// back button
			layout.Rigid(func(gtx C) D {
				gtx.Constraints.Min.X = 75
				gtx.Constraints.Max.X = 75
				return layout.Inset{Bottom: unit.Dp(10), Top: unit.Dp(20)}.Layout(gtx, material.Button(p.app.Theme, &p.backButton, "<- Back").Layout)
			}),
		)
	})
}

// Layout the right-hand side (RHS) of the page body
func (p *Page) layoutRHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Flexed(100, material.Label(p.app.Theme, unit.Dp(18), "Use the \"back\" button to revisit any selections to change them.").Layout),
			// TODO: add support for distro release descriptions, and list them here:
		)
	})
}

func (p *Page) Layout(gtx C) D {
	if p.uiface == nil {
		p.err = fmt.Errorf("pages/confirm.New: unable to init, ui is nil")
	}

	// process any button clicks on page
	if p.backButton.Clicked() {
		p.app.Router.Pop()
	}

	if p.confirmButton.Clicked() {
		device := p.app.Config.Devices[p.uiface.Release.DeviceKey]
		switch device.Flasher {
		case "raw":
			p.app.Router.Pages["preflash"] = raw.New(p.app, p.uiface.Release.DeviceKey, p.uiface.Image)
			p.app.Router.Push("preflash")
			p.app.Window.Invalidate()
		case "fastboot":
			p.err = fmt.Errorf("flash method not implemented yet: %q", device.Flasher)
		case "uuu":
			p.err = fmt.Errorf("flash method not implemented yet: %q", device.Flasher)
		default:
			p.err = fmt.Errorf("unknown flash method: %q", device.Flasher)
		}

	}

	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Horizontal,
					Spacing: layout.SpaceBetween,
				}.Layout(gtx,
					// UI content left hand side
					layout.Flexed(60, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutLHS)
					}),
					// UI content right hand side
					layout.Flexed(40, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutRHS)
					}),
				)
			}),
			// footer
			layout.Flexed(100, func(gtx C) D {
				return p.footer.Layout(gtx, p.app.Theme)
			}),
		}...)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx, sections...)
}
