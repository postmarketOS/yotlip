package select_release

import (
	"fmt"
	"image/color"
	"log"

	"gioui.org/font/gofont"
	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/richtext"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/device"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/pages/select_ui"
	"gitlab.com/postmarketOS/yotlip/ui"
)

type Page struct {
	app           *core.App
	relButtons    []*releaseButton
	header        *ui.Header
	footer        *ui.Footer
	scrollWrap    *ui.ScrollWrap
	device        *device.Device
	backButton    widget.Clickable
	richTextState richtext.InteractiveText
	err           error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

type releaseButton struct {
	ui.TextButton
	release *distro.Release
}

func New(app *core.App, dev *device.Device) *Page {
	p := &Page{
		app:        app,
		header:     ui.NewHeader(app),
		footer:     ui.NewFooter(app),
		scrollWrap: &ui.ScrollWrap{Theme: app.Theme},
		device:     dev,
	}
	return p
}

func (p *Page) Update(data interface{}) bool {
	// stub
	return false
}

// Layout the left-hand side (LHS) of the page body
func (p *Page) layoutLHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(8)}.Layout(gtx,
					material.H5(p.app.Theme, "2. Select a "+p.app.Distro.GetName()+" release:").Layout)
			}),
			// release buttons
			layout.Flexed(100, func(gtx C) D {
				var children []func(gtx C) D
				for _, c := range p.relButtons {
					c := c
					children = append(children, func(gtx C) D {
						return c.Layout(gtx, p.app.Theme)
					})
				}
				p.scrollWrap.Items = children
				return p.scrollWrap.Layout(gtx)
			}),
			// back button
			layout.Rigid(func(gtx C) D {
				gtx.Constraints.Min.X = 75
				gtx.Constraints.Max.X = 75
				return layout.Inset{Bottom: unit.Dp(10), Top: unit.Dp(20)}.Layout(gtx, material.Button(p.app.Theme, &p.backButton, "<- Back").Layout)
			}),
		)
	})
}

// Layout the right-hand side (RHS) of the page body
func (p *Page) layoutRHS(gtx C) D {
	font := gofont.Collection()[0].Font
	normalFont := font
	boldFont := font
	boldFont.Weight = text.Bold

	var spans []richtext.SpanStyle = []richtext.SpanStyle{
		{
			// TODO: improve this text, and put strings like this in some central location?
			Content: "This step lists the releases that " + p.app.Distro.GetName() + " supports for the:\n",
			Size:    unit.Dp(18),
			Font:    normalFont,
			Color:   color.NRGBA{A: 255},
		},
		{
			Content: p.device.DisplayName + ".",
			Size:    unit.Dp(18),
			Font:    boldFont,
			Color:   color.NRGBA{A: 255},
		},
	}

	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Flexed(100, richtext.Text(&p.richTextState, p.app.Theme.Shaper, spans...).Layout),
			// TODO: add support for distro release descriptions, and list them here:
		)
	})
}

func (p *Page) Layout(gtx C) D {
	if len(p.app.Distro.ListReleases(p.device.Key)) == 0 {
		p.err = fmt.Errorf("select_release: no releases found for this device")
	}

	// process any button clicks on page
	if p.backButton.Clicked() {
		p.app.Router.Pop()
	}

	if len(p.relButtons) == 0 {
		// if a valid release was given on the cmdline, use it
		// this check is done here so that it happens only once instead of on
		// every frame redraw
		if *p.app.Options["release"] != "" {
			var rel *distro.Release
			for _, r := range p.app.Distro.ListReleases(p.device.Key) {
				if r.Name == *p.app.Options["release"] {
					rel = r
					break
				}
			}
			if rel != nil {
				log.Printf("pages/select_release: auto-selecting release from commandline: %q", *p.app.Options["release"])
				p.app.Router.Pages["select_ui"] = select_ui.New(p.app, rel)
				p.app.Router.Push("select_ui")
				p.app.Window.Invalidate()
			} else {
				log.Printf("pages/select_release: ignoring unsupported/unknown release set on commandline: %q", *p.app.Options["release"])
			}
		}

		for _, r := range p.app.Distro.ListReleases(p.device.Key) {
			rb := releaseButton{}
			rb.Text = r.Name
			rb.release = r
			button := ui.Button{X: 200, Y: 200, ButtonTemplate: &rb}
			rb.Button = button

			p.relButtons = append(p.relButtons, &rb)
		}
	}

	// process any button clicks on page
	for _, button := range p.relButtons {
		if button.Clicked() {
			p.app.Router.Pages["select_ui"] = select_ui.New(p.app, button.release)
			p.app.Router.Push("select_ui")
			p.app.Window.Invalidate()
			break
		}
	}

	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Horizontal,
					Spacing: layout.SpaceBetween,
				}.Layout(gtx,
					// UI content left hand side
					layout.Flexed(60, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutLHS)
					}),
					// UI content right hand side
					layout.Flexed(40, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutRHS)
					}),
				)
			}),
			// footer
			layout.Flexed(100, func(gtx C) D {
				return p.footer.Layout(gtx, p.app.Theme)
			}),
		}...)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx, sections...)
}
