package raw

import (
	"fmt"
	"image/color"
	"log"

	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gitlab.com/postmarketOS/yotlip/config"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/flasher"
	"gitlab.com/postmarketOS/yotlip/pages/install"
	"gitlab.com/postmarketOS/yotlip/pkgs/diskinfo"
	"gitlab.com/postmarketOS/yotlip/ui"
)

type Page struct {
	app              *core.App
	header           *ui.Header
	footer           *ui.Footer
	device           *config.Device
	image            *distro.DistroImage
	nextButton       widget.Clickable
	refreshButton    widget.Clickable
	disks            []*diskinfo.DiskInfo
	diskList         widget.List
	diskButtonsGroup widget.Enum
	diskButtons      []material.RadioButtonStyle
	err              error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

func New(app *core.App, deviceKey string, image *distro.DistroImage) *Page {
	device := app.Config.Devices[deviceKey]
	p := &Page{
		app:    app,
		header: ui.NewHeader(app),
		footer: ui.NewFooter(app),
		device: &device,
		image:  image,
	}

	p.diskList.Axis = layout.Vertical

	disks, err := diskinfo.GetDisks(false)
	if err != nil {
		p.err = fmt.Errorf("preflash/raw: error getting available disks: %w", err)
	}
	p.disks = disks
	p.genDiskButtons()

	return p
}

func (p *Page) Update(data interface{}) bool {
	// not implemented, page doesn't recieve messages
	return false
}

// Layout the left-hand side (LHS) of the page body
func (p *Page) layoutLHS(gtx C) D {
	var msg string
	if len(p.disks) == 0 {
		msg = "No valid disks found for flashing."
		p.diskButtonsGroup.Value = ""
	} else {
		msg = "Select removable disk to use for installation:"
	}
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			// content header text
			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(8)}.Layout(gtx,
					material.H5(p.app.Theme, msg).Layout)
			}),
			// Always show 'refresh' button so users can insert a new disk
			layout.Rigid(func(gtx C) D {
				return layout.W.Layout(gtx, material.Button(p.app.Theme, &p.refreshButton, "Refresh").Layout)
			}),
			// list of disks
			layout.Flexed(100, func(gtx C) D {
				return material.List(p.app.Theme, &p.diskList).Layout(gtx, len(p.diskButtons), func(gtx C, i int) D {
					return p.diskButtons[i].Layout(gtx)
				})
			}),
			layout.Rigid(func(gtx C) D {
				if p.diskButtonsGroup.Value != "" {
					return layout.UniformInset(unit.Dp(20)).Layout(gtx, material.Button(p.app.Theme, &p.nextButton, "Begin Installation").Layout)
				}
				return layout.Dimensions{}
			}),
		)
	})
}

// Layout the right-hand side (RHS) of the page body
func (p *Page) layoutRHS(gtx C) D {

	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		l := material.Label(p.app.Theme, unit.Dp(18), "Warning: all data on the selected disk will be lost!")
		l.Font.Weight = text.Bold
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			layout.Rigid(material.Label(p.app.Theme, unit.Dp(18), "This device uses a flash method that requires an SD card. Pay special attention to which disk you select.").Layout),
			layout.Flexed(100, l.Layout),
		)
	})
}

func (p *Page) genDiskButtons() {
	p.diskButtons = nil
	for _, d := range p.disks {
		text := fmt.Sprintf("%s %s (%0.2f GiB, %s)", d.Vendor, d.Model, d.SizeGiB, d.DeviceFile)
		p.diskButtons = append(p.diskButtons, material.RadioButton(p.app.Theme, &p.diskButtonsGroup, d.DeviceFile, text))
	}
}

func (p *Page) Layout(gtx C) D {

	if p.nextButton.Clicked() {
		rf := flasher.RawFlasher{
			Disk: p.diskButtonsGroup.Value,
		}
		p.app.Router.Pages["install"] = install.New(p.app, &rf, p.image)
		p.app.Router.Push("install")
		p.app.Window.Invalidate()
	}

	if *p.app.Options["disk"] != "" {
		rf := flasher.RawFlasher{
			Disk: *p.app.Options["disk"],
		}
		log.Printf("pages/preflash/raw: using disk from commandline: %q", *p.app.Options["disk"])
		p.app.Router.Pages["install"] = install.New(p.app, &rf, p.image)
		p.app.Router.Push("install")
		p.app.Window.Invalidate()
	}

	if p.refreshButton.Clicked() {
		p.disks, p.err = diskinfo.GetDisks(false)
		p.genDiskButtons()
	}

	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Horizontal,
					Spacing: layout.SpaceBetween,
				}.Layout(gtx,
					// UI content left hand side
					layout.Flexed(60, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutLHS)
					}),
					// UI content right hand side
					layout.Flexed(40, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutRHS)
					}),
				)
			}),
			// footer
			layout.Flexed(100, func(gtx C) D {
				return p.footer.Layout(gtx, p.app.Theme)
			}),
		}...)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx, sections...)
}
