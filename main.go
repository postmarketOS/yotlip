package main

import (
	"embed"
	gioApp "gioui.org/app"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/unit"
	"git.sr.ht/~gioverse/skel/router"
	"git.sr.ht/~gioverse/skel/scheduler"
	"git.sr.ht/~gioverse/skel/window"
	"gitlab.com/postmarketOS/yotlip/config"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/pages/select_device"
	"log"
	"os"
)

//go:embed imgs/*
var resources embed.FS

type (
	C = layout.Context
	D = layout.Dimensions
)

func main() {
	bus := scheduler.NewWorkerPool()
	w := window.NewWindower(bus)
	go func() {
		w.Run()
		os.Exit(0)
	}()
	window.NewWindow(bus, loop, gioApp.Title("Linux Phone Installer"),
		gioApp.MinSize(unit.Dp(800), unit.Dp(600)))
	gioApp.Main()

}

func loop(w *gioApp.Window, bus scheduler.Connection) error {
	var ops op.Ops

	var config config.Config
	if err := config.Parse("./config.yml"); err != nil {
		log.Fatal("unable to parse config file: ", err)
	}

	// TODO: pass path to some config, or work dir?
	app, err := core.NewApp(w, config, &resources, bus)
	if err != nil {
		log.Fatal("failed to initialize application: ", err)
	}

	// note: other pages added as they are needed at runtime since the
	// constructors for them depend on runtime values
	app.Router.Pages = map[string]router.Page{
		"select_device": select_device.New(&app),
	}
	app.Router.Push("select_device")

	for {
		select {
		case e := <-w.Events():
			switch e := e.(type) {
			case system.DestroyEvent:
				return e.Err
			case system.FrameEvent:
				gtx := layout.NewContext(&ops, e)
				app.Router.Layout(gtx)
				e.Frame(gtx.Ops)
			}
		case update := <-bus.Output():
			// send message to all pages
			app.Router.Update(update)
			w.Invalidate()
		}
	}
}
