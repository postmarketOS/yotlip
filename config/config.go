package config

import (
	yaml "gopkg.in/yaml.v3"
	"io/ioutil"
)

type Release struct {
	Regexp      string `yaml:"regexp"`
	Description string `yaml:"description"`
}

type Config struct {
	Distros map[string]Distro        `yaml:"distros"`
	Devices map[string]Device        `yaml:"devices"`
	UIs     map[string]UserInterface `yaml:"interfaces"`
}

type Distro struct {
	DisplayName string             `yaml:"display_name"`
	ImagesURL   string             `yaml:"images_url"`
	Logo        string             `yaml:"logo"`
	Releases    map[string]Release `yaml:"releases"`
}

type Device struct {
	DisplayName string `yaml:"display_name"`
	Flasher     string `yaml:"flasher"`
	Photo       string `yaml:"photo"`
}

type UserInterface struct {
	DisplayName string `yaml:"display_name"`
	Description string `yaml:"description"`
	Photo       string `yaml:"photo"`
}

func (c *Config) Parse(file string) error {
	// TODO: take io.Reader?

	// TODO: check version in config file, compare app's supported config
	// version, and handle mismatches

	d, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(d, c); err != nil {
		return err
	}

	// TODO: return a config 'object'
	return nil
}
